## 1.11 Next Day

**Voraussetzung**:
Vorlesung zu ausgewählten Sprachkonstrukten der Sprache Java.

**Ziel**:
Anwendung von `switch-case`-Konstrukten, Aufzählungstypen (Enums) sowie Ausnahmebehandlung.

**Dauer**:
< 2 Stunden

## Aufgabe
Schreiben Sie eine ausführbare Klasse `NextDay`, welche für ein gegebenes Datum (Tag/Monat/Jahr) das Datum des nächsten Tages berechnet und ausgibt.

Programmieren Sie dazu eine Klasse `Date` mit den Attributen Tag, Monat und Jahr. Es soll möglich sein, ein Objekt dieser Klasse durch einen Konstruktor mit genau diesen Parametern (Tag, Monat, Jahr) zu erzeugen.

Die Monate sollen durch Konstanten eines Enums `Month` repräsentiert werden. Die Klasse `Date` soll die Methode `public Date getNextDay()` besitzen, die ein neues Objekt der Klasse `Date` zurückgibt, das den nächsten Tag im Kalender repräsentiert.

Falls versucht wird, ein `Date`-Objekt mit einer fehlerhaften Datumskonstellation (z.B. 32.12.2018) zu erzeugen, soll eine Ausnahme geworfen werden. Dazu erstellen Sie eine eigene Klasse `DateException`, die von `java.lang.Exception` erbt.

Berücksichtigen Sie bei der Berechnung auch Schaltjahre:
- Ein Jahr ist ein Schaltjahr, wenn es durch 4, aber nicht zugleich durch 100 teilbar ist.
- Ist das Jahr durch 400 teilbar, ist es dennoch ein Schaltjahr.

Überlegen Sie, welche weiteren (evtl. privaten) Methoden zur Berechnung des Folgetages benötigt werden.

Die Klasse `NextDay` erzeugt ein `Date`-Objekt für ein gegebenes Datum (z.B. 12.12.2004), gibt dieses auf der Standardausgabe aus, ruft die Methode `getNextDay()` auf diesem Objekt auf und gibt das resultierende Objekt ebenfalls auf der Standardausgabe aus. Die Ausgabe der Klasse sollte wie folgt aussehen:

```
Old date: 12.12.2024
New date: 13.12.2024
```
